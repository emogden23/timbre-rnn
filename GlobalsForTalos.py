# Hacks to get around restrictive interface Talos Scan() to use test data on Keras model callback
# Only needed for Talos hyperparameter work, not needed otherwise
# Could pass these as parameters, but don't want to risk multiple copies of large dictionary that
# need to be managed

test_data = []
KNOWN_CLASSES = []
USE_EXISTING_WEIGHTS = False

SERIALIZED_PARAMS_LOG_FILENAME = './model/talos/ser_param_log.pickle'
SERIALIZED_PARAMS_WEIGHTS_PREFIX = './model/talos/weights_'

HAMMING_LOG_FILENAME = './results/talos/hamming_temp.csv'
