
import csv
import os
import pdb

import numpy as np
from tensorflow.keras.callbacks import Callback
from timbre_misc_funcs import calc_conf_mat_metrics

class HammingLossCallback(Callback):
    def __init__(self, test_data, logpath, rounding_threshold=0.5, class_names='',
                 print_results=False):
        super(Callback, self).__init__()
        self.x_test = test_data[0]
        self.y_true = test_data[1]
        self.logname = logpath
        self.rounding_threshold = rounding_threshold
        self.class_names = class_names
        self.print_results = print_results
        self.hamming_loss = -1

    def on_train_begin(self, logs=None):
        self.hamming_loss = -1

    def on_train_end(self, logs=None):
        # Calculate from (global) test data since any 'Keras Metric' approach would use training or
        # validation data instead
        y_pred = self.model.predict(self.x_test)

        (per_class_scores, self.hamming_loss, conf_mat_data) = \
            calc_conf_mat_metrics(self.y_true, y_pred, self.rounding_threshold,
                                  self.class_names, self.print_results)
        (precision, recall, F1) = per_class_scores

        string_formatting = '{0:.4f}'
        class_precision = '; '.join(str.format(string_formatting, precision_i)
                                    for precision_i in precision)
        class_recall = '; '.join(str.format(string_formatting, recall_i) for recall_i in recall)
        class_F1 = '; '.join(str.format(string_formatting, F1_i) for F1_i in F1)

        mean_precision = str.format(string_formatting, precision.mean())
        mean_recall = str.format(string_formatting, recall.mean())
        mean_F1 = str.format(string_formatting, F1.mean())

        if not os.path.isfile(self.logname):
            with open(self.logname, 'w', newline='') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter=',')
                csvwriter.writerow(('class_precision', 'mean_precision', 'class_recall',
                                    'mean_recall', 'class_F1', 'mean_F1', 'hamming_loss'))

        # No index/row number as ID, but should be same order as Talos csv
        with open(self.logname, 'a', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',')
            csvwriter.writerow((class_precision, mean_precision, class_recall, mean_recall,
                               class_F1, mean_F1, str.format(string_formatting, self.hamming_loss)))

        return self.hamming_loss
