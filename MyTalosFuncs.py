# Contain modifications, additions to Talos functions

# TODO: possible code contributions:
# general parameter dictionary format: allow for nested dictionaries
#   - would allow for specific dictionaries to be passed to hidden_layers style functions
#   - would need to alter the parameter grid function to account for this: flatten with prefixes?
#   - Alternatively, some way to get around the HARDCODED parameter keys that force the same
#     settings across all cases
# Improved lr_normalizer
#   - Relies strictly on getting a pure-Keras optimizer, silently fails otherwise
#   - Minimum fix: add an 'else' with a warning message that the optimizer wasn't handled/recognized
#   - Better fix for me: also consider tensorflow-wrapped versions
# Add LSTM version of hidden_layers()
#   - Just adds new keyword checks, changed main layer type
#   - Could have a more generic way to pass in the desired layer type (would work for LSTM, GRU,
#     etc.), but different parameter names make than a problem

import pdb

from talos.utils.exceptions import TalosParamsError, TalosModelError

def hidden_layers(model, params, last_neuron):
    from tensorflow.keras.layers import Dense, Dropout  # EO: added 'tensorflow.'
    from talos.model.network_shape import network_shape
    '''HIDDEN LAYER Generator

    NOTE: 'first_neuron', 'dropout', and 'hidden_layers' need
    to be present in the params dictionary.

    Hidden layer generation for the cases where number
    of layers is used as a variable in the optimization process.
    Handles things in a way where any number of layers can be tried
    with matching hyperparameters.'''

    try:
        kernel_initializer = params['kernel_initializer']
    except KeyError:
        kernel_initializer = 'glorot_uniform'

    try:
        kernel_regularizer = params['kernel_regularizer']
    except KeyError:
        kernel_regularizer = None

    try:
        bias_initializer = params['bias_initializer']
    except KeyError:
        bias_initializer = 'zeros'

    try:
        bias_regularizer = params['bias_regularizer']
    except KeyError:
        bias_regularizer = None

    try:
        use_bias = params['use_bias']
    except KeyError:
        use_bias = True

    try:
        activity_regularizer = params['activity_regularizer']
    except KeyError:
        activity_regularizer = None

    try:
        kernel_constraint = params['kernel_constraint']
    except KeyError:
        kernel_constraint = None

    try:
        bias_constraint = params['bias_constraint']
    except KeyError:
        bias_constraint = None

    # check for the params that are required for hidden_layers
    for param in ['shapes', 'first_neuron', 'dropout']:
        try:
            params[param]
        except KeyError as err:
            if err.args[0] == param:
                raise TalosParamsError("hidden_layers requires '" + param + "' in params")

    layer_neurons = network_shape(params, last_neuron)

    for i in range(params['hidden_layers']):
        model.add(Dense(layer_neurons[i],
                        activation=params['activation'],
                        use_bias=use_bias,
                        kernel_initializer=kernel_initializer,
                        kernel_regularizer=kernel_regularizer,
                        bias_initializer=bias_initializer,
                        bias_regularizer=bias_regularizer,
                        activity_regularizer=activity_regularizer,
                        kernel_constraint=kernel_constraint,
                        bias_constraint=bias_constraint))

        model.add(Dropout(rate=params['dropout']))


# Modification of talos.model.layers.hidden_layers() for CuDNNLSTMs
def hidden_LSTM_layers(model, params, last_neuron):
    from tensorflow.keras.layers import CuDNNLSTM, Dropout

    '''LSTM LAYER Generator
    Alternate parameter names for this:
    'shapes' -> 'lstm_shapes'
    'first_neuron' -> 'lstm_first_neuron'
    'dropout' -> 'lstm_dropout'
    'hidden_layers' -> 'num_lstm_layers'
    Others: 'lstm_' prefix
    '''

    try:
        kernel_initializer = params['lstm_kernel_initializer']
    except KeyError:
        kernel_initializer = 'glorot_uniform'

    try:
        kernel_regularizer = params['lstm_kernel_regularizer']
    except KeyError:
        kernel_regularizer = None

    try:
        recurrent_initializer = params['lstm_recurrent_initializer']
    except KeyError:
        recurrent_initializer = 'orthogonal'

    try:
        recurrent_regularizer = params['lstm_recurrent_regularizer']
    except KeyError:
        recurrent_regularizer = None

    try:
        bias_initializer = params['lstm_bias_initializer']
    except KeyError:
        bias_initializer = 'zeros'

    try:
        bias_regularizer = params['lstm_bias_regularizer']
    except KeyError:
        bias_regularizer = None

    try:
        unit_forget_bias = params['lstm_unit_forget_bias']
    except KeyError:
        unit_forget_bias = True

    try:
        activity_regularizer = params['lstm_activity_regularizer']
    except KeyError:
        activity_regularizer = None

    try:
        kernel_constraint = params['lstm_kernel_constraint']
    except KeyError:
        kernel_constraint = None

    try:
        recurrent_constraint = params['lstm_recurrent_constraint']
    except KeyError:
        recurrent_constraint = None

    try:
        bias_constraint = params['lstm_bias_constraint']
    except KeyError:
        bias_constraint = None

    # check for the params that are required for hidden_layers
    for param in ['lstm_shapes', 'lstm_first_neuron', 'lstm_dropout']:
        try:
            params[param]
        except KeyError as err:
            if err.args[0] == param:
                raise TalosParamsError("hidden_LSTM_layers requires '" + param + "' in params")

    layer_neurons = lstm_network_shape(params, last_neuron)

    for i in range(params['num_lstm_layers']):
        model.add(CuDNNLSTM(layer_neurons[i],
                        kernel_initializer=kernel_initializer,
                        kernel_regularizer=kernel_regularizer,
                        recurrent_initializer=recurrent_initializer,
                        recurrent_regularizer=recurrent_regularizer,
                        bias_initializer=bias_initializer,
                        bias_regularizer=bias_regularizer,
                        activity_regularizer=activity_regularizer,
                        unit_forget_bias=unit_forget_bias,
                        kernel_constraint=kernel_constraint,
                        recurrent_constraint=recurrent_constraint,
                        bias_constraint=bias_constraint,
                        return_sequences=True,
                        return_state=False,
                        go_backwards=False,
                        stateful=False))

        model.add(Dropout(rate=params['lstm_dropout']))


# Talos-provided network_shape() is also HARDCODED for specific parameter names
def lstm_network_shape(params, last_neuron):
    '''Provides the ability to include network shape in experiments. If params
    dictionary for the round contains float value for params['shapes'] then
    a linear contraction towards the last_neuron value. The higher the value,
    the fewer layers it takes to reach lesser than last_neuron.

    Supports three inbuilt shapes 'brick', 'funnel', and 'triangle'.


    params : dict
         Scan() params for a single roundself.
    last_neuron : int
         Number of neurons on the output layer in the Keras model.
    '''

    import numpy as np
    import pdb

    layers = params['num_lstm_layers']
    shape = params['lstm_shapes']
    first_neuron = params['lstm_first_neuron']
    out = []
    n = first_neuron

    # the cases where an angle is applied
    if isinstance(shape, float):

        for i in range(layers):

            n *= 1 - shape

            if n > last_neuron:
                out.append(int(n))
            else:
                out.append(last_neuron)

    # the case where a rectantular shape is used
    elif shape == 'brick':
        out = [first_neuron] * layers

    elif shape == 'funnel':
        for i in range(layers + 1):
            n -= int((first_neuron - last_neuron) / layers)
            out.append(n)
        # pdb.set_trace()
        out.pop(-1)

    elif shape == 'triangle':
        out = np.linspace(first_neuron,
                          last_neuron,
                          layers+2,
                          dtype=int).tolist()
        out.pop(0)
        out.pop(-1)
        out.reverse()

    return out


def lr_normalizer_for_TF(lr, optimizer):
    from tensorflow.keras.optimizers import Adadelta, Adagrad, Adam, Adamax, Nadam, RMSprop, SGD

    """Assuming a default learning rate 1, rescales the learning rate
    such that learning rates amongst different optimizers are more or less
    equivalent.

    Parameters
    ----------
    lr : float
        The learning rate.
    optimizer : keras optimizer
        The optimizer. For example, Adagrad, Adam, RMSprop.
    """

    if optimizer == Adadelta:
        pass
    elif optimizer == SGD or optimizer == Adagrad:
        lr /= 100.0
    elif optimizer == Adam or optimizer == RMSprop:
        lr /= 1000.0
    elif optimizer == Adamax or optimizer == Nadam:
        lr /= 500.0
    else:
        # TODO: should this be a ParamsError or a ModelError? Since optimizer may not be variable,
        # model makes more sense
        raise TalosModelError("Unhandled optimizer type in lr_normalizer_for_TF: " + str(optimizer) +
                              ". Only tensorflow-housed keras optimizers are handled.")
    return lr

def evaluate_talos_report(report, param_keys, n_select=3, metrics=['hamming_loss', 'mean_F1'],
                          sort_orders=[True, False]):
    for metric, sort_order in zip(metrics, sort_orders):
        best_metric_data = report.data.sort_values(metric, ascending=sort_order)[:n_select]
        n_actual = min(n_select, len(best_metric_data.index))

        print('\nBest {0:d} models, {1} (row index: value; parameters):'.format(n_actual, metric))
        print(best_metric_data[metric].to_dict())
        for ii in best_metric_data.index.values:
            curr_data = best_metric_data.loc[ii, param_keys]
            print('\n#{0:d}:'.format(ii))
            print(curr_data)
            # print(curr_data.to_dict())    # more compact, but less readable
