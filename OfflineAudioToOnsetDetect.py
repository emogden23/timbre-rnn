# Use onset detection to simplify labeling of output data
# Also has silence detection (instead of more complex offset detection)

from aubio import onset, source
import numpy as np

def get_onset_data(audio_filename, win_s, show_plot):
    """Use Aubio's onset detection for easier data labeling. Currently has crude
       off-set (silence) detection as well."""

    # TODO: mess around with aubio's predefined formulas?
    hop_s = win_s // 2                   # hop size, increment between fft windows

    samplerate = 0    # EO: let source() determine it

    # reduce to mono for single input stream
    audio = source(audio_filename, samplerate, hop_s, channels=1)
    if samplerate == 0:
        samplerate = audio.samplerate

    ons = onset("default", win_s, hop_s, samplerate)

    # list of onsets, in samples
    onsets = []

    # list of off-sets, in samples
    offsets = []
    MOV_AVG_WINDOW = 9
    mov_avg_buffer = []
    SILENCE_THRESHOLD = 0.03    # tuned manually
    onset_was_recent = False

    # storage for plotted data
    desc = []
    tdesc = []
    allsamples_max = np.zeros(0,)
    DOWNSAMPLE = 2  # to plot n samples / hop_s

    # total number of frames read
    total_frames = 0

    # TODO: onset detection not working well for alto-sax - make less sensitive,
    # or manually clean up?

    while True:
        samples, read = audio()
        # Note: extra 'onset_was_recent' check works for silence-based off-set,
        # but won't work well for multi-instrument pieces...
        if ons(samples) and not onset_was_recent:
            print("%f" % (ons.get_last_s()))
            onsets.append(ons.get_last())     # Note: stored as frame number, not time
            onset_was_recent = True
        # keep some data to plot it later
        new_maxes = (abs(samples.reshape(hop_s//DOWNSAMPLE, DOWNSAMPLE))).max(axis=0)
        allsamples_max = np.hstack([allsamples_max, new_maxes])
        desc.append(ons.get_descriptor())
        tdesc.append(ons.get_thresholded_descriptor())

        # Off-set detection: currently way too simple, just checking sample maxes
        # TODO: need to adjust towards all frequency measures + 'perceived loudness'
        # (filter the audio)
        # TODO: only just checks all or nothing, doesn't check for offset of
        # different pitches/instruments - only works for single instrument samples

        # For moving average/off-set detection
        # 'downsample' in new maxes producing multiple values (not sure the purpose);
        # take first of the two most recent values
        if not len(mov_avg_buffer):
            mov_avg_buffer = allsamples_max[-2]*np.ones(MOV_AVG_WINDOW)
        else:
            mov_avg_buffer = np.roll(mov_avg_buffer, 1, 0)
            mov_avg_buffer[0] = allsamples_max[-2]
        max_sample_movavg = np.mean(mov_avg_buffer)
        if max_sample_movavg < SILENCE_THRESHOLD and onset_was_recent:
            offsets.append(total_frames)
            onset_was_recent = False

        # Appears to handle all of the audio, not just those with complete hop size
        total_frames += read
        if read < hop_s:
            break

    # Write-to-txt here, so, plot can be used to adjust the label file
    pure_filename = (audio_filename.split('/')[-1]).split('.')[0]
    with open(''.join(('audio_samples/parsed_onsets/', pure_filename, '.txt')),
              'w') as txt_file:
        txt_file.write(''.join((str(total_frames), '\n')))
        on_off_events = sorted(set(onsets).union(set(offsets)))
        for frame_num in on_off_events:
            string_list_to_write = []
            string_list_to_write.append(''.join((str(frame_num), '::On:')))
            # TODO: add check if same instrument if present in both onset and
            # offset - if so, skip writing
            if frame_num in onsets:
                # TODO: ideally, expand this to more instruments
                string_list_to_write.append('x')
            string_list_to_write.append(';Off:')
            if frame_num in offsets:
                string_list_to_write.append('x')
            string_list_to_write.append(';\n')
            string_to_write = ''.join(string_list_to_write)
            txt_file.write(string_to_write)

    if show_plot:
        # do plotting
        import matplotlib.pyplot as plt
        allsamples_max = (allsamples_max > 0) * allsamples_max
        allsamples_max_times = [float(t)*hop_s/DOWNSAMPLE/samplerate
                                for t in range(len(allsamples_max))]
        plt1 = plt.axes([0.1, 0.75, 0.8, 0.19])
        plt2 = plt.axes([0.1, 0.1, 0.8, 0.65], sharex=plt1)

        # Plot 'amplitude'
        plt.rc('lines', linewidth='.8')
        plt1.plot(allsamples_max_times, allsamples_max, '-b')
        plt1.plot(allsamples_max_times, -allsamples_max, '-b')

        for stamp in onsets:
            stamp /= float(samplerate)
            plt1.plot([stamp, stamp], [-1., 1.], '-r')
        for stamp in offsets:
            stamp /= float(samplerate)
            plt1.plot([stamp, stamp], [-1., 1.], '--', color='tab:purple')

        plt1.axis(xmin=0., xmax=max(allsamples_max_times))
        plt1.xaxis.set_visible(False)
        plt1.yaxis.set_visible(False)

        # Plot ??
        desc_times = [float(t)*hop_s/samplerate for t in range(len(desc))]
        desc_max = max(desc) if max(desc) != 0 else 1.
        desc_plot = [d/desc_max for d in desc]
        plt2.plot(desc_times, desc_plot, '-g')
        tdesc_plot = [d/desc_max for d in tdesc]

        for stamp in onsets:
            stamp /= float(samplerate)
            plt2.plot([stamp, stamp], [min(tdesc_plot), max(desc_plot)], '-r')
        for stamp in offsets:
            stamp /= float(samplerate)
            plt2.plot([stamp, stamp], [min(tdesc_plot), max(desc_plot)],
                      '--', color='tab:purple')

        plt2.plot(desc_times, tdesc_plot, '-y') # small amplitude line
        plt2.axis(ymin=min(tdesc_plot), ymax=max(desc_plot))
        plt.xlabel('time (s)')

        # TODO: getting hung up/freezing here - ??
        plt.show()

    return (onsets, offsets)


if __name__ == '__main__':
    import os
    import tkinter as tk
    from tkinter import filedialog

    from timbre_misc_funcs import audio_extension_parse_files

    WINDOW_SIZE = 512        # TODO: setting from aubio demo, adjust?
    tk_root = tk.Tk()
    tk_root.withdraw()

    AUDIO_DIR = tk.filedialog.askdirectory(title='Select the directory containing audio '
                                                    'files to process for onset events.')
    FOUND_FILES = sorted([afile for afile in os.listdir(AUDIO_DIR)
                         if os.path.isfile(os.path.join(AUDIO_DIR, afile))])
    BASE_AUDIO_FILES = audio_extension_parse_files(FOUND_FILES)
    if not BASE_AUDIO_FILES:
        sys.exit('No supported audio files found - stopping...')
    AUDIO_FILES = [os.path.join(AUDIO_DIR, curr_file) for curr_file in BASE_AUDIO_FILES]

    for curr_file in AUDIO_FILES:
        onoff_data = get_onset_data(curr_file, WINDOW_SIZE, show_plot=True)
