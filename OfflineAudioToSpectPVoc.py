# Convert audio file to FFT/time-frequency data as input to RNNs; based on
# 'demo_spectrogram.py' from aubio library

import glob
import os

from aubio import pvoc, source, float_type
import matplotlib.pyplot as plt
from numpy import zeros, log10, vstack

def get_spectrogram_data(audio_filename, fft_win_size):
    """(CURRENTLY DEBUG) Calculate STFT audio information; superceded by
       OfflineAudioToSpectWavelet.py."""
    # TODO: mess around with aubio's predefined formulas?
    hop_s = fft_win_size // 2                   # hop size, increment between fft windows
    fft_s = fft_win_size // 2 + 1               # spectrum bins; sets number of of fft bins

    # TODO: can bins be logarithmic? check sonic visualiser

    samplerate = 0    # EO: let source() determine it

    # reduce to mono for single input stream
    audio = source(audio_filename, samplerate, hop_s, channels=1)
    if samplerate == 0:
        samplerate = audio.samplerate
    pv = pvoc(fft_win_size, hop_s)                     # phase vocoder
    specgram = zeros([0, fft_s], dtype=float_type)     # numpy array to store spectrogram

    # analysis
    while True:
        samples, read = audio()            # read file; samples type: numpy.ndarray

        # print(samples)    # type: numpy.ndarray, shape: hop_s x indeterminate
        # input('asd')

        # phase vocoder (pv): uses short-time FT/FFT
        specgram = vstack((specgram, pv(samples).norm))   # store new norm vector
        if read < audio.hop_size:
            break

    # print(type(specgram.T))   # type: numpy.ndarray
    # print(specgram.T.shape)

    specplt = plt.imshow(log10(specgram.T + .001), origin='bottom', aspect='auto')

    specax = specplt.axes
    specax.axis([0, len(specgram), 0, len(specgram[0])])

    # show axes in Hz and seconds
    time_step = hop_s / float(samplerate)
    total_time = len(specgram) * time_step
    outstr = "total time: %0.2fs" % total_time
    print outstr + ", samplerate: %.2fkHz" % (samplerate / 1000.)
    n_xticks = 10
    n_yticks = 10

    def get_rounded_ticks(top_pos, step, n_ticks):
        """Linear axis tick placement, part of aubio demo"""
        top_label = top_pos*step
        # get the first label
        ticks_first_label = top_pos*step/n_ticks
        # round to the closest .1
        ticks_first_label = round(ticks_first_label * 10.)/10.
        # compute all labels from the first rounded one
        ticks_labels = [ticks_first_label*n for n in range(n_ticks)] + [top_label]
        # get the corresponding positions
        ticks_positions = [ticks_labels[n]/step for n in range(n_ticks)] + [top_pos]
        # convert to string
        ticks_labels = ["%.1f" % x for x in ticks_labels]
        # return position, label tuple to use with x/yticks
        return ticks_positions, ticks_labels

    # apply to the axis
    x_ticks, x_labels = get_rounded_ticks(len(specgram), time_step, n_xticks)
    y_ticks, y_labels = get_rounded_ticks(len(specgram[0]),
                                          (samplerate/1000./2.)/len(specgram[0]),
                                          n_yticks)
    specax.set_xticks(x_ticks)
    specax.set_yticks(y_ticks)
    specax.set_xticklabels(x_labels)
    specax.set_yticklabels(y_labels)
    specax.set_ylabel('Frequency (kHz)')
    specax.set_xlabel('Time (s)')
    specax.set_title(os.path.basename(audio_filename))
    for item in ([specax.title, specax.xaxis.label, specax.yaxis.label] +
                 specax.get_xticklabels() + specax.get_yticklabels()):
        item.set_fontsize('x-small')

    return specgram.T, specplt

if __name__ == '__main__':
    # audio_filename = 'stuttering-violin_87bpm_E_minor.wav'
    # audio_filename = 'cymbal05.wav'
    # audio_filename = 'trombone-pack_120bpm_C_major.wav'
    # audio_filename = 'cowbell.wav'

    FFT_WINDOW_SAMPLES = 512        # TODO: setting from aubio demo, adjust?
    AUDIO_FILES = sorted(glob.glob('audio_samples/raw_audio/*.wav'))
    for curr_file in AUDIO_FILES:
        spec_data, fig = get_spectrogram_data(curr_file, FFT_WINDOW_SAMPLES)
        # display graph
        plt.show()

        # TODO: need to refine, save spectrogram data as input; maybe need a
        # loudness/perception filter to change 'power' to 'volume', reduce
        # impact of low frequency elevated power
            # Note: this shouldn't be necessary for actual data processing/network
            # training, just for debugging plots - don't edit specgram?
            # aubio.db_spl(): computes loudness across all frequencies, not a filter
        # TODO: change spectrogram data to logarithmicly-spaced center frequencies?
        # Better matches human perception and smaller network needed
        # (https://www.composersdesktop.com/pdf/phasevocoder.pdf); is very high precision
        # at high frequencies needed? Maybe use wavelet-based method (PyWavelets)?

        # TODO: look up 'onset detection', see if that can be used to simplify
        # output labeling
