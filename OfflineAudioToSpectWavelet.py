# Convert audio file to wavelet/time-frequency data as input to RNNs; based on
# 'demo_spectrogram.py' from aubio library and http://ataspinar.com/2018/12/21/
# Also has phase vocoder/STFT stuff for comparison/wavelet selection

import os
import pdb

import aubio as ab
import cmocean # for phase colormap
import matplotlib.pyplot as plt
import numpy as np
import pywt

from timbre_misc_funcs import *

def get_wavelet_data(audio_filename, fft_win_size, show_plot=False):
    """Calculate STFT and wavelet-based audio information to use
       as input for Timbre network."""

    hop_s = fft_win_size // 2                   # hop size, increment between fft windows
    fft_s = fft_win_size // 2 + 1               # spectrum bins; sets number of of fft bins

    samplerate = 0    # EO: let source() determine it

    # reduce to mono for single input stream (averages the two channels)
    audio = ab.source(audio_filename, samplerate, hop_s, channels=1)
    if samplerate == 0:
        samplerate = audio.samplerate

    pv = ab.pvoc(fft_win_size, hop_s)                     # phase vocoder
    specgram = np.zeros([0, fft_s], dtype=ab.float_type)     # numpy array to store spectrogram

    all_samples = np.zeros([], dtype=ab.float_type)
    while True:
        samples, read = audio()            # read file; samples type: numpy.ndarray
        # 'samples' type: numpy.ndarray, shape: hop_s x indeterminate

        # phase vocoder (pv): uses short-time FT/FFT
        specgram = np.vstack((specgram, pv(samples).norm))   # store new norm vector

        all_samples = np.hstack((all_samples, samples))   # store new norm vector
        if read < audio.hop_size:
            break

    # Trim excess samples attached to the audio
    # Remove extra values (zeros) attached at the end
    # Note: some audio files have 1 extra sample at beginning (e.g. string_melody_02)
    # Don't drop other samples, just trim input/output data later in RNN code
    n_zero_padded_vals = audio.hop_size - read
    all_samples = np.delete(all_samples, np.s_[-n_zero_padded_vals:])

    # Wavelet stuff with all_samples
    # "morl" produces peaks/valleys with phase mismatch
    # "cmorB-C": B = time-freq tradeoff, high B has more freq precision; C is
    # center frequency, ??? if data is fed in without considering dimensions
    # (sampling rate)
    # cmor seems to have different proportionality constant, doubling C seems to
    # double the constant (2xfreq for same scales), B doesn't change it
    wl_choice = "cmor1-1"
    wl_samplepd = 1/samplerate

    # TODO: maybe alter other wavelet properties (orthogonal, symmetry, ??)
    # How much can be edited with pre-defined wavelet?
    # 'cmor1-1' settings: (bi)orthogonal=False, lower/upper_bound (precision)=-/+8,
    # fbsp_order=(nothing?), symmetry=asymmetric

    # wl_min_scale = wavelet_s2f_bisection(wl_choice, 20000, wl_samplepd)
    # wl_max_scale = wavelet_s2f_bisection(wl_choice, 5, wl_samplepd)
    # wl_scale = np.linspace(wl_min_scale, wl_max_scale, 8192)

    # Using linearly spaced scales creates too few bins at high frequencies
    # Octave scaling? Geometric series creates wider bins at higher frequencies
    # How to properly distribute in scale space?
    # If scale is inversely proportional to frequency, then scale becomes
    # constant/(f = f_0*2^(i/12))
    # Template: 'wavelet_s2f_bisection(wl_choice, 20000, wl_samplepd)'
    # Based on endpoints, "morl" constant = 35831.25;
    # "cmor1-1" = 44100 (= 1*samplerate = 1/samplepd)
    # "cmor1-2" = 88200 (=2*samplerate)
    # So, scale = constant/"octave_range"

    # Use existing musical frequencies as basis (start at 440 Hz)
    # -64 to +67: 10.9 Hz to 21.1 kHz
    # -52 to +67: 21.8 Hz to 21.1 kHz; use this for now
    freq_set = np.array([440*2.0**(i/12) for i in np.arange(-52, 68)])

    f2s_constants = {"morl": 35831.25, "cmor1-1": 44100, "cmor2-1": 44100,
                     "cmor4-1": 44100, "cmor1-2": 88200}
    f2s_const = f2s_constants[wl_choice]
    # if not f2s_const:
    #     sys.exit("Current wavelet choice ({0}) doesn't have a defined "
    #                 "frequency-to-scale constant.".format(wl_choice))
    wl_scale = f2s_const/freq_set

    # freq_test = pywt.scale2frequency(wl_choice, wl_scale)/wl_samplepd
    # print(freq_test)
    # fs_plot = plt.plot(wl_scale, freq_test, '.')
    # plt.ylabel('Freq (Hz)')
    # plt.xlabel('Wavelet Scaling Value')
    # plt.show()
    # print('asd')

    wl_coefs, wl_freqs = pywt.cwt(all_samples, wl_scale, wl_choice, wl_samplepd)
    # don't use log/dB for magnitude - produces weird artifacts?
    # Use 'astype()' rather than usign universal arguments to prevent reduced precision
    # when working with np.complex128's (affects both calculation and output)
    wl_coefs_mag = np.absolute(wl_coefs).astype(np.float16, casting='same_kind')
    wl_coefs_phase = np.angle(wl_coefs).astype(np.float16, casting='same_kind')
    print('Wavelet scaleogram values (time histories of magnitude, phase) calculated')

    # With test frequencies of 220, 440, 880 Hz: wavelet activity in right area,
    # but seems more widespread over multiple frequencies than FFT (spectral leakage?)
    # Try different window sizes/methods?

    # Also, unable to tell the difference between normal and amplitude-modulated
    # 440 Hz in wavelet, but possible in FFT - need to understand the amplitudes/outputs better
    # Maybe a matplotlib.imshow interpolation/filter issue present in plotting?

    # (OLD) Also, don't understand the colors - try different wavelet families?
    # Getting maxes/mins as I pass in time for constant 220 Hz - currently using
    # continuous real Morlet, phase mismatch could be causing this
    # (https://dsp.stackexchange.com/questions/7911/)
    # Instead, use complex Morlet for match regardless of phase? (OLD)

    # Complex morlet (1-1) may have resolved beats/peaks in frequency band over time,
    # but amplitude changes (440 hz flanger) still doesn't seem right - different parameters?
    # Also, magnitude value increases/accumulates over time until saturation, occurs
    # faster at higher frequencies (220 vs 440 vs 880)
    # Occurs slower at higher B, but also decreases amplitude nearby - less
    # spectral leakage with tighter bandwidth, makes sense; also affects phase,
    # but I don't understand it (looks like stretching it over longer time for higher B)

    # Still, wavelets seem to do better than phase vocoder/stft for crows, car?
    # (FFT has too few low frequency bins)
    # No excess amlitude across wide band of frequencies, matches better with
    # Sonic Visualiser output (more likely to be correct than my FFT implementation)

    if show_plot:
        # TODO: slow for large images, figure out how to use vispy or bokeh
        time_freq_plots, (ax_fft, ax_wl_mag, ax_wl_phase) = \
                    plt.subplots(3, 1, sharex=True, squeeze=True)
        time_freq_plots.set_size_inches((8, 10))

        specplt = ax_fft.imshow(np.log10(specgram.T + .001), origin='bottom',
                                aspect='auto', interpolation='nearest')

        specax = specplt.axes
        specax.axis([0, len(specgram), 0, len(specgram[0])])

        # show axes in Hz and seconds
        time_step = hop_s / float(samplerate)
        # total_time = len(specgram) * time_step
        # outstr = "total time: %0.2fs" % total_time
        # print(outstr + ", samplerate: %.2fkHz" % (samplerate / 1000.))
        n_xticks = 10
        n_yticks = 10

        def get_rounded_ticks(top_pos, step, n_ticks):
            """Linear axis tick placement, part of aubio demo"""
            top_label = top_pos*step
            # get the first label
            ticks_first_label = top_pos*step/n_ticks
            # round to the closest .1
            ticks_first_label = round(ticks_first_label * 10.)/10.
            # compute all labels from the first rounded one
            ticks_labels = [ticks_first_label*n for n in range(n_ticks)] + [top_label]
            # get the corresponding positions
            ticks_positions = [ticks_labels[n]/step for n in range(n_ticks)] + [top_pos]
            # convert to string
            ticks_labels = ["%.1f" % x for x in ticks_labels]
            # return position, label tuple to use with x/yticks
            return ticks_positions, ticks_labels

        # apply to the axis
        x_ticks, x_labels = get_rounded_ticks(len(specgram), time_step, n_xticks)
        y_ticks, y_labels = get_rounded_ticks(len(specgram[0]),
                                              (samplerate/1000./2.)/len(specgram[0]),
                                              n_yticks)
        specax.set_xticks(x_ticks)
        specax.set_yticks(y_ticks)
        specax.set_xticklabels(x_labels)
        specax.set_yticklabels(y_labels)
        specax.set_ylabel('Frequency (kHz)')
        specax.set_title(os.path.basename(audio_filename) + ': STFT')
        for item in ([specax.title, specax.xaxis.label, specax.yaxis.label] +
                     specax.get_xticklabels() + specax.get_yticklabels()):
            item.set_fontsize('x-small')

        # pdb.set_trace()

        # Tkinter/matplotlib/imshow doesn't like float16 consistently, plot float32's
        sc_mag_plt = ax_wl_mag.imshow(wl_coefs_mag.astype(np.float32, casting='same_kind'),
                                      aspect='auto', origin='lower',
                                      cmap='Spectral', interpolation='nearest')
        sc_mag_ax = sc_mag_plt.axes

        def findOverlap(a, b, rtol=1e-05, atol=1e-08):
            """#Find tick placement for frequency labels on scale axis;
            https://stackoverflow.com/a/32514613"""
            from itertools import islice
            ovr_a = []
            ovr_b = []
            start_b = 0
            for i, ai in enumerate(a):
                for j, bj in islice(enumerate(b), start_b, None):
                    if np.isclose(ai, bj, rtol=rtol, atol=atol, equal_nan=False):
                        ovr_a.append(i)
                        ovr_b.append(j)
                    elif bj > ai: # (more than tolerance)
                        break # all the rest will be farther away
                    else: # bj < ai (more than tolerance)
                        start_b += 1 # ignore further tests of this item
            return (ovr_a, ovr_b)

        y_plt_labels = [440*2.0**(i) for i in np.arange(-4, 6)]
        yticks, dummy = findOverlap(wl_freqs, y_plt_labels)
        sc_mag_ax.set_yticks(yticks)
        sc_mag_ax.set_yticklabels(y_plt_labels)
        sc_mag_ax.set_ylabel('Frequency (kHz)')
        sc_mag_ax.set_title('Wavelet Scaleogrm (magnitude, dB)')

        sc_phase_plt = ax_wl_phase.imshow(wl_coefs_phase.astype(np.float32, casting='same_kind'),
                                          aspect='auto', origin='lower',
                                          cmap=cmocean.cm.phase, interpolation='nearest')
        sc_phase_ax = sc_phase_plt.axes
        sc_phase_ax.set_yticks(yticks)
        sc_phase_ax.set_yticklabels(y_plt_labels)
        sc_phase_ax.set_ylabel('Frequency (kHz)')
        sc_phase_ax.set_xlabel('Time (s)')
        sc_phase_ax.set_title('Wavelet Scaleogrm (phase)')

        cbar_mag_ax = time_freq_plots.add_axes([0.915, 0.395, 0.03, 0.2])
        cbar_phase_ax = time_freq_plots.add_axes([0.915, 0.125, 0.03, 0.2])
        time_freq_plots.colorbar(sc_mag_plt, cax=cbar_mag_ax, orientation="vertical")
        time_freq_plots.colorbar(sc_phase_plt, cax=cbar_phase_ax, orientation="vertical")

        for item in ([sc_mag_ax.title, sc_mag_ax.xaxis.label, sc_mag_ax.yaxis.label,
                      sc_phase_ax.title, sc_phase_ax.xaxis.label, sc_phase_ax.yaxis.label] +
                     sc_mag_ax.get_xticklabels() + sc_mag_ax.get_yticklabels() +
                     sc_phase_ax.get_xticklabels() + sc_phase_ax.get_yticklabels() +
                     cbar_mag_ax.get_yticklabels() + cbar_phase_ax.get_yticklabels()):
            item.set_fontsize('x-small')

        time_freq_plots.show()

        # pdb.set_trace()
        # input('asd')

        # TODO: add reconstructed signal plot/error/?? to see how well chosen
        # wavelet matches the input?

    return wl_coefs_mag, wl_coefs_phase

if __name__ == '__main__':
    import tkinter as tk
    from tkinter import filedialog

    # audio_filename = 'stuttering-violin_87bpm_E_minor.wav'
    # audio_filename = 'cymbal05.wav'
    # audio_filename = 'trombone-pack_120bpm_C_major.wav'
    # audio_filename = 'cowbell.wav'

    FFT_WINDOW_SAMPLES = 512        # TODO: setting from aubio demo, adjust?

    tk_root = tk.Tk()
    tk_root.withdraw()

    AUDIO_DIR = tk.filedialog.askdirectory(title='Select the directory containing audio '
                                                    'files to process for scaleograms.')
    FOUND_FILES = sorted([afile for afile in os.listdir(AUDIO_DIR)
                         if os.path.isfile(os.path.join(AUDIO_DIR, afile))])
    BASE_AUDIO_FILES = audio_extension_parse_files(FOUND_FILES)
    if not BASE_AUDIO_FILES:
        sys.exit('No supported audio files found - stopping...')
    AUDIO_FILES = [os.path.join(AUDIO_DIR, curr_file) for curr_file in BASE_AUDIO_FILES]

    # Since the input data is likely too big to use current drive, user defines it
    OUT_DIRNAME = tk.filedialog.askdirectory(title='Select the directory to store '
                                             'scaleogram *.npy files.')

    tk_root.destroy()

    for curr_file in AUDIO_FILES:
        print('Parsing {0}'.format(curr_file))
        wavelet_mags, wavelet_phases = get_wavelet_data(curr_file, FFT_WINDOW_SAMPLES, True)
        basename_parts = os.path.basename(curr_file).split('.')

        # Keep data shape as (scale bins x samples), but reduce memory size
        # Current choice: binary-written files (*.npy), half precision: 10.6 MB/sec

        # Remove original file extension from name - can throw off alphabetical sort in certain
        # cases, could cause mismatch between input and output data
        # np.save(''.join((out_dirname, '/mag_', basename_parts[0], '_',
        #                 basename_parts[1])), wavelet_mags)
        np.save(''.join((OUT_DIRNAME, '/mag_', basename_parts[0])), wavelet_mags)
        np.save(''.join((OUT_DIRNAME, '/phase_', basename_parts[0])), wavelet_phases)
        print('Wrote scaleogram data to csv files for {0}'.format(curr_file))
        # pdb.set_trace()
        # TODO: refine scaleogram data; maybe need a loudness/perception filter to change
        # 'power' to 'volume', reduce impact of elevated power at low frequencies
            # Note: this shouldn't be necessary for actual data processing/network
            # training, just for debugging plots - don't edit specgram?
