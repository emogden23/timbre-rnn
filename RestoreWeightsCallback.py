# Callback for restoring model weights from checkpoint (hdf5) file at the end of training
# Make sure that any evaluation/done with the model uses the best weights

import pdb

from tensorflow.keras.callbacks import Callback, ModelCheckpoint

class RestoreWeightsCallback(Callback):
    def __init__(self, filename):
        super(Callback, self).__init__()
        self.filename = filename

    def on_train_end(self, logs=None):
        # Model appears to be shallow-copied, as thsi affects the model at highest scope
        self.model.load_weights(self.filename)
        return 1

class RestoreBestWeightsCallback(ModelCheckpoint):
    """Modified checkpoint callback that restores best weights at the end of training"""
    def __init__(self, filepath, monitor='val_loss', verbose=0, mode='auto', period=1):
        save_best_only = True
        save_weights_only = True
        super(RestoreBestWeightsCallback, self).__init__(filepath, monitor, verbose, save_best_only,
                                                         save_weights_only, mode, period)

    # def on_epoch_end(): re-use ModelCheckpoint's, save the best

    def on_train_end(self, logs=None):
        self.model.load_weights(self.filepath)
        return 1
