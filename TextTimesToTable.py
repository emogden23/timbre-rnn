# Convert text file of time data into RNN-compatible format

# Note: currently only handles frame numbers (indices in table)

# TODO: cache/save the data if it takes too long to generate?

# TODO: would this generate one-to-one/same length data as original?
# should do FFT first to see what type of data is produced

import numpy as np

def TextTimesToTable(filename, known_cols, show_plot=False):
    """Convert events from OfflineAudioToOnsetDetect.py to fully populated
       data matrix for use in network training."""
    with open(filename) as txt_file:
        event_frames = []
        onset_events = []
        offset_events = []
        for line in txt_file:
            if line.find(':') >= 0:
                line_colon_split = line.split(':')
                event_frames.append((int(line_colon_split[0])))
                onset_string = line_colon_split[3].split(';')[0]
                onset_events.append(onset_string.split(','))
                offset_string = line_colon_split[4].split(';')[0]
                offset_events.append(offset_string.split(','))
            else:
                # Assume abnormal line (probably first) is number of samples
                # Need to add one sample (start from 0 OR scaleogram artifact,
                # not exactly clear)
                num_samples = int(line) + 1

    # print(num_samples)
    # print(event_frames)
    # print(onset_events)
    # print(offset_events)

    # Check that on/offset labels are in known_cols; if not, error out
    # (Considered growing table as new parts added, but only works within
    # files; would need to go back and reparse files to add new columns)
    # Instead, print message
    # https://stackoverflow.com/a/41127279
    no_class_errors = True
    for x in np.setdiff1d(onset_events, known_cols):
        if x:   # workaround for empty list, has to be a better way to do this
            print('\nError: Unknown class {0} in onsets for {1}\n'.format(x, filename))
            no_class_errors = False
    for x in np.setdiff1d(offset_events, known_cols):
        if x:
            print('\nError: Unknown class {0} in offsets for {1}\n'.format(x, filename))
            no_class_errors = False

    if no_class_errors:
        data = np.zeros((num_samples, len(known_cols)), dtype=np.uint8)
        row_data = np.zeros((len(known_cols)), dtype=np.uint8)
        event_count = 0
        for frame in event_frames:
            for x in onset_events[event_count]:
                if x:   # workaround empty item in list again...
                    row_data[known_cols.index(x)] = 1
            for x in offset_events[event_count]:
                if x:
                    row_data[known_cols.index(x)] = 0
            # TODO: set next block of rows to row_data
            if event_count < len(event_frames)-1:
                data[frame:event_frames[event_count+1], :] = row_data
            else:
                data[frame:, :] = row_data
            event_count += 1
    else:
        data = np.zeros(len(known_cols))
    # create table, start populating based on known_cols
    # if frame_num has on/off event, edit row data to use
    # if event not known from previous data, throw error
    # if no event, repeat last row

    if show_plot:
        import matplotlib.pyplot as plt
        for i in np.arange(0, len(known_cols)):
            plt.plot(np.arange(0, num_samples), data[:, i])
        plt.legend(known_cols)
        plt.show()

    return data, no_class_errors

if __name__ == '__main__':
    import glob
    KNOWN_CLASSES = ['noise', 'violin', 'trumpet'] # 'none' case is all 0's
    EVENT_FILES = sorted(glob.glob('audio_samples/parsed_onsets/cleaned/*.txt'))
    text_expand_success = True
    for curr_file in EVENT_FILES:
        expanded_data, curr_success = TextTimesToTable(curr_file, KNOWN_CLASSES, False)
        text_expand_success = text_expand_success and curr_success
    print(text_expand_success)
