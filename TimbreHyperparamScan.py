# Function with standard interface to use with Talos huperparameter scanning
# Highly redundant with TimbreNN functions, but lists-of-parameters + standard model interface are
# a pain...

import os
import pdb

import talos
import tensorflow as tf

import GlobalsForTalos as gft
from HammingLossCallbackForTalos import HammingLossCallback
import MyTalosFuncs
from RestoreWeightsCallback import RestoreBestWeightsCallback
from timbre_misc_funcs import *

def timbre_for_talos_scan(x_train, y_train, x_val, y_val, params):
    # Possible fix for Tensorflow error when starting second model permutation: maybe old
    # model layouts aren't cleared properly?
    # https://github.com/jaungiers/Multidimensional-LSTM-BitCoin-Time-Series/issues/1#issuecomment-389609457
    tf.keras.backend.clear_session()

    num_features = x_train.shape[1]
    num_timesteps = x_train.shape[2]
    num_classes = y_train.shape[1]

    per_batch_raw_input_shape = (num_features, num_timesteps)
    inputs = tf.keras.layers.InputLayer(input_shape=per_batch_raw_input_shape,
                                        batch_size=params['batch_size'], name='input_layer',
                                        dtype=tf.float32)
    input_to_RNN_permute = tf.keras.layers.Permute((2, 1),
                                                   input_shape=per_batch_raw_input_shape)
    model = tf.keras.models.Sequential()
    model.add(inputs)
    model.add(input_to_RNN_permute)

    MyTalosFuncs.hidden_LSTM_layers(model, params, params['lstm_last_neuron'])

    # TODO: if multiple parallel paths used (RNN + CNN), use keras.layers.Concatenate()

    MyTalosFuncs.hidden_layers(model, params, params['last_neuron'])

    probabilities = tf.keras.layers.Dense(num_classes, activation='sigmoid',
                                          dtype=tf.float32, name="probabilities")
    outputs_to_raw_shape = tf.keras.layers.Permute((2, 1),
                                                   input_shape=(num_timesteps,
                                                                num_classes))
    model.add(probabilities)
    model.add(outputs_to_raw_shape)

    loss = tf.keras.losses.BinaryCrossentropy(from_logits=False)

    # FIXED: lr_normalizer expects keras.optimizers, not a tensorflow-housed one; learning rate
    # wasn't being converted
    optimizer = params['optimizer'](MyTalosFuncs.lr_normalizer_for_TF(params['learning_rate'],
                                    params['optimizer']), clipnorm=params['grad_clip'])

    metrics = [tf.keras.metrics.BinaryAccuracy(threshold=params['rounding_threshold']),
               tf.keras.metrics.Precision(),
               tf.keras.metrics.Recall()
               ]
    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    model.summary()

    if gft.USE_EXISTING_WEIGHTS:
        weights_index = serialized_params_to_index(params,
                                                   gft.SERIALIZED_PARAMS_LOG_FILENAME)
        weights_filename = gft.SERIALIZED_PARAMS_WEIGHTS_PREFIX + str(weights_index) + '.hdf5'
        if os.path.exists(weights_filename):
            model.load_weights(weights_filename)
        else:
            print('\nIntermediate model weights not found, starting from scratch\n')
    else:
        weights_filename = gft.SERIALIZED_PARAMS_WEIGHTS_PREFIX + 'temp.hdf5'
        print('\nNot considering existing weights, final weights won''t be stored\n')

    model.reset_states()

    # Added load_weights() at training end to ModelCheckpoint
    best_weights_clbk = RestoreBestWeightsCallback(weights_filename, monitor='val_loss', period=1)
    vld_stop_clbk = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-6,
                                                       patience=7, mode='auto')
    lr_reduce_clbk = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                                          patience=3, cooldown=1,
                                                          verbose=1)
    # Callback order preserved, so this should be after RestoreBestWeights
    hamming_loss_clbk = HammingLossCallback(gft.test_data, gft.HAMMING_LOG_FILENAME,
                                            params['rounding_threshold'], gft.KNOWN_CLASSES,
                                            print_results=True)
    training_callbacks = [
                            best_weights_clbk,
                            vld_stop_clbk,
                            lr_reduce_clbk,
                            hamming_loss_clbk
                         ]

    out = model.fit(x_train, y_train, params['batch_size'], params['epochs'],
                   callbacks=training_callbacks, validation_data=(x_val, y_val),
                   verbose=1)

    # No explicit evaluation, handled by HammingLossCallback

    return out, model
