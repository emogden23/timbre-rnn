
import os
import pdb

import line_profiler
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import tensorflow as tf

from timbre_misc_funcs import *

class TimbreNN():
    """Neural network class for identifying instruments present in audio. Based
    on movie review sentiment and character prediction RNNs in 'Python Machine
    Learning' by Raschka and Mirijalili, 2nd ed. Mostly updated to Tensorflow API 1.13,
    (some updates not clear) and includes design intended a nVidia Geforce GTX 1080 Ti."""
    def __init__(self, num_classes, num_features, num_timesteps, params, batch_size=1):
        # Number of outputs (noise, instruments; not 'null')
        self.num_classes = num_classes

        # Input shape: num_features (scaleogram bins) x timesteps (consecutive time samples) x
        # batch_size (groups of timesteps, determined by input dimensions)
        self.num_features = num_features
        self.num_timesteps = num_timesteps

        # Number of batches (each batch num_features x num_timesteps) to use at a time in training
        self.batch_size = batch_size

        self.lstm_size = params['lstm_first_neuron']
        self.num_lstm_layers = params['num_lstm_layers']

        # TODO: add convolution layers in parallel? (tone/harmonics correlation, similar to image
        # processing?)

        self.learning_rate = params['learning_rate']

        # TODO: need to tune dropout rate, gradient clip
        self.dropout = params['lstm_dropout']
        self.grad_clip = params['grad_clip']

        self.rounding_threshold = params['rounding_threshold']

        self.g = tf.Graph()
        with self.g.as_default():

            # tf.set_random_seed(623)

            self.model = self.build()
            self.init_op = tf.global_variables_initializer()

        # May need this before saving to file
        with tf.Session(graph=self.g) as sess:
            sess.run(self.init_op)

    def build(self):
        # FP32 for speed
        # Keep input shape natural/intuitive outside neural network context
        per_batch_raw_input_shape = (self.num_features, self.num_timesteps)
        inputs = tf.keras.layers.InputLayer(input_shape=per_batch_raw_input_shape,
                                            batch_size=self.batch_size, name='input_layer',
                                            dtype=tf.float32)

        # TODO: how much would things speed up if this was done ahead of time to the data?
        input_to_RNN_permute = tf.keras.layers.Permute((2, 1),
                                                       input_shape=per_batch_raw_input_shape)

        # Need 'return_sequences=True' to return all outputs per time step (batch_size,
        # num_timesteps, num_features), not just last one (batch_size, (1), num_features)
        # TODO: make activation a placeholder/hyperparameter?
        # CUDA-based method WAY faster than normal LSTM, use this if possible
        LSTM_template = tf.keras.layers.CuDNNLSTM(self.lstm_size, return_sequences=True,
                                                  stateful=False)

        # TODO: not sure if dropout is correctly disabled during testing, but looks like it
        # should be...?
        dropout_post_LSTM = tf.keras.layers.Dropout(self.dropout)

        # TODO: if multiple parallel paths used (RNN + CNN), use keras.layers.Concatenate()

        fc_post_LSTM = tf.keras.layers.Dense(units=self.lstm_size,
                                              activation='tanh', dtype=tf.float32,
                                              name='combined_outs')

        # Softmax layer removed to allow for multiple high-probability classes at the same
        # time (multiple instruments simultaneously = multilabel classification)
        # Adding this narrows output domain to (0, 1) - matches label data
        probabilities = tf.keras.layers.Dense(self.num_classes, activation='sigmoid',
                                              dtype=tf.float32, name="probabilities")

        # per_batch_raw_output_shape = (self.num_classes, self.num_timesteps)
        outputs_to_raw_shape = tf.keras.layers.Permute((2, 1),
                                                       input_shape=(self.num_timesteps,
                                                                    self.num_classes))

        model = tf.keras.models.Sequential()
        model.add(inputs)
        model.add(input_to_RNN_permute)
        for dummy in range(self.num_lstm_layers):
            model.add(LSTM_template)
        model.add(dropout_post_LSTM)
        model.add(fc_post_LSTM)
        model.add(probabilities)
        model.add(outputs_to_raw_shape)

        # Make sure BCE knows inputs (perumuted probabilities) aren't logits - allows probabilities
        # to be returned, predicted, and used easier
        loss = tf.keras.losses.BinaryCrossentropy(from_logits=False)

        # After reading, (https://www.tensorflow.org/api_docs/python/tf/clip_by_global_norm ,
        # https://stackoverflow.com/a/43486487), need to clip by norm (not by value). After digging
        # through source code, 'clipnorm' seems to be the same as this (or at least very close)
        # TODO: add hyperparameters to optimizer?
        optimizer = tf.keras.optimizers.Adam(self.learning_rate, clipnorm=self.grad_clip)

        # Based on Yu-Yang's answer in https://stackoverflow.com/questions/45799474/ , binary
        # accuracy should be the one I want. But, including others just to see
        # TODO: maybe add additional metrics (Ch 6 of book: Precision(), TruePositives(), ROC?)
        # Should find ones appropriate for multilabel classification:
        # - True positives: whether it identifies instruments (and what fraction)
        # - False positives: instrument overlap/confusion
        # - False negatives: overwhelmed/can't pick stuff up?
        metrics = [tf.keras.metrics.BinaryAccuracy(threshold=self.rounding_threshold),
                   # tf.keras.metrics.CategoricalAccuracy(),
                   tf.keras.metrics.Precision(),
                   tf.keras.metrics.Recall()]

        # TODO: can't use top_k_categorical in metrics list, since not a full class -
        # calculate manually during train()?
        # tf.keras.metrics.top_k_categorical_accuracy(k=5)

        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
        model.summary()

        return model

    def train(self, train_data, num_epochs, validation_data, checkpoint_dir='./ckpt/',
                 starting_model_weights='', new_model_weights=''):
        if not os.path.exists(checkpoint_dir):
                os.mkdir(checkpoint_dir)

        with tf.Session(graph=self.g) as sess:
            sess.run(self.init_op)

            # Note: If error with "object 'dropout' doesn't exist', may be trying to load
            # an incomplete model save
            if os.path.exists(starting_model_weights):
                # save_weights() has better compatibility for both LSTM and CuDNNLSTM
                # WARNING: initial testing shows that CuDNNLSTM weights can be applied to normal
                # LSTM, and vice versa - different number of trainable parameters, but similar loss
                # characteristics. May be OK, or may have dangerous edge cases
                self.model.load_weights(starting_model_weights)

                # TODO: add try-catch: if model import fails, ask (Y/n) to use existing model;
                # if not, sys.exit()
            else:
                print('\nIntermediate model weights not loaded, starting from scratch\n')

            self.model.reset_states()

            # Checkpoints also have to be weights-only
            model_ckpt_clbk = tf.keras.callbacks.ModelCheckpoint(
                ''.join((checkpoint_dir, 'timbre_cp.{epoch:02d}.hdf5')), save_best_only=False,
                save_weights_only=True, period=5)
            lr_reduce_clbk = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                                                  patience=3, cooldown=1,
                                                                  verbose=1)
            # Possible log keys for EarlyStopping: "loss, binary_accuracy, categorical_accuracy,
            # val_* (repeated - same metrics for validation set)" - seems to be 'loss' +
            # stuff added in metrics for model.compile() ; use val_*, loss or binary accuracy?
            vld_stop_clbk = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-6,
                                                               patience=7, mode='auto')
            training_callbacks = [model_ckpt_clbk, lr_reduce_clbk, vld_stop_clbk]

            # TODO: need to figure out how to specify dropout value/placeholder with new format
            # TODO: is model.reset_states() being called? make custom callback for this?
            self.model.fit(train_data[0], train_data[1], self.batch_size, num_epochs,
                           callbacks=training_callbacks, validation_data=validation_data)

            # Cannot save an empty model, so make this part of train()
            if new_model_weights and os.path.exists(os.path.dirname(new_model_weights)):
                self.model.save_weights(new_model_weights)
            else:
                tf.keras.models.save_weights(self.model, 'temp_weights.hdf5')
                print('\nIssue with new model weights, saving them locally as ''temp_weights''\n')

    def predict(self, predict_x, model_weights_filename='', return_prob=False, show_plot=False,
                class_names=''):
        with tf.Session(graph=self.g) as sess:

            # TODO: make weight loading a function?
            if os.path.exists(model_weights_filename):
                # See train() for notes about this behavior
                self.model.load_weights(model_weights_filename)
            else:
                print('\nOutside model weights not loaded, using current values for prediction\n')

            self.model.reset_states()

            # TODO: is dropout disabled?
            predict_y = self.model.predict(predict_x)

            rounding_threshold_storage = self.rounding_threshold
            if not return_prob:
                predict_y = binary_round_by_threshold(predict_y, self.rounding_threshold)
            else:
                self.rounding_threshold = 0

            if show_plot:
                self.plot_results(predict_y, np.arange(predict_y.shape[1]), [], class_names, 3,
                                  downsample=200)

        self.rounding_threshold = rounding_threshold_storage

        return predict_y

    def evaluate_simple(self, test_x, test_y, model_weights_filename=''):
        # Use model.evaluate() and model-assigned metrics - may be useful for overal perf, but
        # no per-class performance analysis
        # Has progress bar and uses GPU, but only up to around 70%
        with tf.Session(graph=self.g) as sess:
            if os.path.exists(model_weights_filename):
                # See train() for notes about this behavior
                self.model.load_weights(model_weights_filename)
            else:
                print('\nOutside model weights not loaded, using current values for evaluation\n')

            self.model.reset_states()

            # TODO: figure out how to speed this up; 'use_multiprocessing?' need generators or
            # keras.utils.Sequence for that
            eval_metrics = self.model.evaluate(test_x, test_y)

            # HARCODED from model metrics[] order
            precision = eval_metrics[2]
            recall = eval_metrics[3]
            F1_score = 2*precision*recall/(precision + recall)
            print(precision, recall, F1_score)

        return (precision, recall, F1_score)

    # @profile
    def evaluate_custom(self, test_x, test_y, model_weights_filename='', show_plot=False,
                 class_names=''):
        # Use self.predict() to get output values, do custom metrics (per-class F1, confusion
        # matrices, Hamming loss)
        # No progress bar in predict(), but uses GPU up to about 80%
        with tf.Session(graph=self.g) as sess:
            if os.path.exists(model_weights_filename):
                # See train() for notes about this behavior
                self.model.load_weights(model_weights_filename)
            else:
                print('\nOutside model weights not loaded, using current values for evaluation\n')

            self.model.reset_states()

            # Confusion matrices round predictions anyways, reduce number of
            # predict() calls by storing probabilities
            print('Running predict() to calculate my own (per-class) metrics')

            # TODO: figure out how to speed this up: 'use_multiprocessing?' need generators or
            # keras.utils.Sequence for that
            pred_y = self.predict(test_x, model_weights_filename, return_prob=True)
            (per_class_scores, hamming_loss, conf_mat_data) = calc_conf_mat_metrics(test_y, pred_y,
                                                                self.rounding_threshold,
                                                                class_names, True)

        if show_plot:
            self.plot_results(pred_y, pc_F1, test_y, class_names, 3, downsample=200)

        return (per_class_scores, hamming_loss, conf_mat_data)

    def plot_results(self, predicted, sorting_basis, actual=None, class_names='', num_plot_pairs=3,
                     downsample=1):
        plot_set_limit = min(num_plot_pairs, len(sorting_basis))

        predicted_2D = flatten_batched_tensor_to_2D(predicted[:, :, ::downsample])
        if actual is None:  # hopefully works for both intended and 'null' inputs
            actual = np.ones(predicted.shape)*np.nan
        actual_2D = flatten_batched_tensor_to_2D(actual[:, :, ::downsample])

        # could add check that predicted, actual match sizes, but assume that's happened prior

        pseudo_time = np.arange(predicted_2D.shape[1])/(44100/downsample) # HARDCODED samplerate
        MIN_SLIDER_WIDTH = 0.25

        best_classes = np.argsort(-sorting_basis)[:plot_set_limit]
        worst_classes = np.argsort(sorting_basis)[:plot_set_limit]

        if not class_names:
            class_names = ['']*predicted_2D.shape[0]

        eval_fig, eval_ax = plt.subplots(2, plot_set_limit, figsize=(12, 8))
        eval_fig.subplots_adjust(left=0.075, right=0.95)
        for i in range(plot_set_limit):
            best_index = best_classes[i]
            worst_index = worst_classes[i]

            eval_ax[0, i].plot(pseudo_time, predicted_2D[best_index, :], 'b-')
            eval_ax[0, i].plot(pseudo_time, actual_2D[best_index, :], 'r--')
            eval_ax[0, i].plot([0, pseudo_time[-1]], [self.rounding_threshold,
                                                      self.rounding_threshold], ':')
            eval_ax[0, i].set_xlim([0, MIN_SLIDER_WIDTH])
            eval_ax[0, i].set_title('Best #{0}: {1}'.format(int(i + 1), class_names[best_index]))

            eval_ax[1, i].plot(pseudo_time, predicted_2D[worst_index, :], 'b-')
            eval_ax[1, i].plot(pseudo_time, actual_2D[worst_index, :], 'r--')
            eval_ax[1, i].plot([0, pseudo_time[-1]], [self.rounding_threshold,
                                                      self.rounding_threshold], ':')
            eval_ax[1, i].set_xlim([0, MIN_SLIDER_WIDTH])
            eval_ax[1, i].set_title('Worst #{0}: {1}'.format(int(i + 1), class_names[worst_index]))

        # Use scrollbar in line plot (https://stackoverflow.com/a/44795507) to keep it
        # manageable and have higher time resolution (see if fast oscillations in
        # probabilites are occurring)
        slider_axis = plt.axes([0.2, 0.01, 0.63, 0.03])
        x_slider = Slider(slider_axis, 'X-min', 0,
                          pseudo_time[-1] - MIN_SLIDER_WIDTH, valinit=0)

        # Note: on_changed/update function can only accept a single float, so made it
        # a local function (shared scope) + move all plots together
        # Only updating the axis limits rather than slicing the data - simpler update
        # function, but may use more resources?
        def x_slider_update(val):
            x_min = x_slider.val
            for i in range(plot_set_limit):
                eval_ax[0, i].set_xlim([x_min, x_min + MIN_SLIDER_WIDTH])
                eval_ax[1, i].set_xlim([x_min, x_min + MIN_SLIDER_WIDTH])
            eval_fig.canvas.draw_idle()

        x_slider.on_changed(x_slider_update)

        plt.show()
