
import os
import pdb
import sys

import line_profiler
import numpy as np
import pandas as pd
import talos
import tensorflow as tf

import MyTalosFuncs
from timbre_misc_funcs import *
from TimbreHyperparamScan import timbre_for_talos_scan
import TimbreNN

def TimbreTrain():
    print('Training Timbre model')
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'    # silence INFO messages for GPU stuff

    KNOWN_CLASSES = ['noise', 'violin', 'trumpet'] # 'none' case is all 0's

    # TODO: what's a good amount of data per batch? Short durations would lead to high dependence
    # on pitch/harmonic analysis ('sustained timbre'), long duration could capture more
    # ringout but would slow training
    # Maybe don't want exact multiple of notes - potential to overlap with small-scale
    # music events, could be more robust to have these occur at different times within batches
    SAMPLE_LENGTH = 10000  # 10000 in 44100: Slightly less than two quarter notes at 120 bpm

    # Amount for 'time subseries' (samples) to overlap
    OVERLAP_AMOUNT = 1000

    # Number of batches to process at a time during training; also drops partial batches from
    # data. Can be changed for prediction/evaluation, so should find good balance between epoch
    # speed and training quality (faster while maintaining metrics)
    # Cursory testing with small dataset (noise, violin, trumpet):
    # batch_size=1: 75s per epoch, 188ms/sample (397 of 397)
    # 2: 48s, 121ms/sample (396)
    # 4: 25s, 65ms/sample (392)
    # 8: 15s, 37ms/sample (392)
    # 16: 10s, 23ms/sample (384)
    # Choice: 8; impact on training accuracy untested, model still a prototype
    BATCH_SIZE = 8

    TEST_FRACTION = 0.2
    VALIDATE_FRACTION = 0.15
    DATA_SPLIT_FRACTIONS = (1.0 - TEST_FRACTION - VALIDATE_FRACTION, TEST_FRACTION,
                            VALIDATE_FRACTION)

    train_data, test_data, validation_data = import_and_partition(KNOWN_CLASSES,
                                                                  DATA_SPLIT_FRACTIONS,
                                                                  SAMPLE_LENGTH, OVERLAP_AMOUNT,
                                                                  BATCH_SIZE)

    NUM_EPOCHS = 100
    # DEBUG: confirm no different parameters betwen normal train and Talos
    hyperparam_defaults = {
           'lstm_first_neuron': 128,
           'num_lstm_layers': 1,
           'lstm_dropout': 0.5,
           # 'activation': ['tanh'],
           'learning_rate': 1e-3,    # scaled to default values (Adam: 1 -> 1e-3)
           'grad_clip': 5,
           'rounding_threshold': 0.5,
    }



    timbre_1 = TimbreNN.TimbreNN(train_data[1].shape[1], train_data[0].shape[1],
                                 SAMPLE_LENGTH, hyperparam_defaults, batch_size=BATCH_SIZE)

    # Log structure for TensorBoard after build()
    with tf.Session(graph=timbre_1.g) as sess:
        sess.run(timbre_1.init_op)
        file_writer = tf.summary.FileWriter(logdir='logs/', graph=timbre_1.g)

    timbre_1_model_weights = './model/timbre1_test_weights.hdf5'
    # timbre_1_model_weights = ''

    # TODO: final model may not have the best weights, need to reload the best ones (see Talos version)
    timbre_1.train(train_data, NUM_EPOCHS, validation_data,
                      starting_model_weights=timbre_1_model_weights,
                      new_model_weights=timbre_1_model_weights)

    (f1, hamming, cm_data) = timbre_1.evaluate_custom(test_data[0], test_data[1],
                                                      timbre_1_model_weights,
                                                      show_plot=True,
                                                      class_names=KNOWN_CLASSES)

    # TODO: generate predictions for new file (no partitioning/shuffling)

def TimbreHyperPScan():
    import GlobalsForTalos as gft

    # Mostly duplicate of TimbreTrain, but includes Talos-driven hyperparameter optimization
    print('Scanning hyperparameters of Timbre model')
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'    # silence INFO messages for GPU stuff

    gft.KNOWN_CLASSES = ['noise', 'violin', 'trumpet'] # 'none' case is all 0's

    SAMPLE_LENGTH = 10000  # 10000 in 44100: Slightly less than two quarter notes at 120 bpm
    OVERLAP_AMOUNT = 1000

    BATCH_SIZE = 8
    NUM_EPOCHS = 100

    TEST_FRACTION = 0.2
    VALIDATE_FRACTION = 0.15
    DATA_SPLIT_FRACTIONS = (1.0 - TEST_FRACTION - VALIDATE_FRACTION, TEST_FRACTION,
                            VALIDATE_FRACTION)

    train_data, gft.test_data, validation_data = import_and_partition(gft.KNOWN_CLASSES,
                                                                  DATA_SPLIT_FRACTIONS,
                                                                  SAMPLE_LENGTH, OVERLAP_AMOUNT,
                                                                  BATCH_SIZE)

    hyperparams = {
                    'lstm_first_neuron': [64, 96, 128, 160],
                    'num_lstm_layers': [1],
                    # 'lstm_dropout': [0.4, 0.5, 0.6],
                    # 'lstm_shapes': ['brick'],
                    # 'lstm_last_neuron': 128, #[128], # seems like this can't be a list...
                    # 'shapes': ['funnel'],
                   # 'first_neuron': [128],
                   # 'activation': ['tanh'],
                   # 'dropout': [0.0],
                   # 'hidden_layers': [1]
                   # 'learning_rate': [1, 1e-1],
                   # 'grad_clip': [3, 5, 7],
                   # 'rounding_threshold': [0.35, 0.5, 0.65]
                   # TODO: optimizer class
    }

    hyperparam_defaults = {
                           # start hidden_LSTM_layers() requisite keys (see Talos' hidden_layers())
                           'lstm_shapes': ['brick'],
                           'lstm_first_neuron': [128],
                           'num_lstm_layers': [1],
                           'lstm_dropout': [0.5],
                           # end hidden_LSTM_layers() requisite keys
                           'lstm_last_neuron': [128], #[128], # seems like this can't be a list...
                           # start Talos hidden_layers() requisite keys
                           # 'shapes' used in network_shape: 'brick', 'funnel', 'triangle', or
                           # floating-point contraction rate (0, 1)
                           'shapes': ['funnel'],
                           'first_neuron': [128],
                           'activation': ['tanh'],
                           'hidden_layers': [1],
                           'dropout': [0.0],
                           'last_neuron': [128],    # Note: NOT the probability outputs/num_classes
                           # end Talos hidden_layers() requisite keys
                           'learning_rate': [1],    # scaled to default values (Adam: 1 -> 1e-3)
                           'grad_clip': [5],
                           'rounding_threshold': [0.5],
                           'optimizer': [tf.keras.optimizers.Adam],
                           # Keep loss metric constant - suited for multilabel classification
                           # Other parameters needed to supply to hyperparameter scan while
                           # maintaining standard interface
                           'epochs': [NUM_EPOCHS],
                           'batch_size': [BATCH_SIZE]
    }

    # Considered using nested dictionaries to get around HARDCODED key names, but Talos probably won't
    # generate parameter permutations properly - back to a bunch of modified Talos functions...
    # hyperparam_defaults = {
    #     'lstm_params': {
    #         'shapes': ['brick'],
    #         'first_neuron': [128],
    #         'dropout': [0.5],
    #         'last_neuron': [128],
    #         'hidden_layers': [1],
    #     },
    #     'final_dense_params': {
    #         # 'shapes' used in network_shape: 'brick', 'funnel', 'triangle', or
    #         # floating-point contraction rate (0, 1)
    #         'shapes': ['funnel'],
    #         'first_neuron': [128],
    #         'activation': ['tanh'],
    #         'dropout': [0.0],
    #         'last_neuron': [128],
    #         'hidden_layers': [1],
    #     }
    #     'learning_rate': [1e-3],
    #     'grad_clip': [5],
    #     'rounding_threshold': [0.5],
    #     'optimizer': [tf.keras.optimizers.Adam],
    #     # Keep loss metric constant - suited for multilabel classification
    #     # Other parameters needed to supply to hyperparameter scan while
    #     # maintaining standard interface
    #     'epochs': [NUM_EPOCHS],
    #     'batch_size': [BATCH_SIZE]
    # }

    for key in hyperparam_defaults:
        if key not in hyperparams:
            hyperparams[key] = hyperparam_defaults[key]

    gft.USE_EXISTING_WEIGHTS = True

    if os.path.isfile(gft.HAMMING_LOG_FILENAME):
        os.remove(gft.HAMMING_LOG_FILENAME)

    dataset_name = './results/talos/timbre_basic'
    exp_num = 1
    talos_out = talos.Scan(train_data[0], train_data[1], hyperparams,
                            x_val=validation_data[0],
                            y_val=validation_data[1],
                            model=timbre_for_talos_scan,
                            shuffle=False,
                            dataset_name=dataset_name,
                            experiment_no=str(int(exp_num)),
                            grid_downsample=1)

    # TODO: report results, find top 3-5 models according to F1 score, Hamming loss
    #
    talos_results_filename = dataset_name + '_' + str(int(exp_num)) + '.csv'
    combined_data = pd.concat([pd.read_csv(f) for f in [talos_results_filename,
                              gft.HAMMING_LOG_FILENAME]], axis=1, ignore_index=False)
    talos_combo_filename = dataset_name + '_' + str(int(exp_num)) + '_combo.csv'
    combined_data.to_csv(talos_combo_filename)

    report = talos.Reporting(talos_combo_filename)
    MyTalosFuncs.evaluate_talos_report(report, [*hyperparams], n_select=3,
                                       metrics=['hamming_loss', 'mean_F1'])

# @profile
def TimbreTest():
    # Similar to TimbreTrain(), but only evaluate the model (all data is test data)
    print('Testing Timbre model')
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

    KNOWN_CLASSES = ['noise', 'violin', 'trumpet']

    SAMPLE_LENGTH = 10000
    OVERLAP_AMOUNT = 1000
    BATCH_SIZE = 32  # doesn't need to match batch_size used for training, but can affect metrics

    # First set used for 'test', not train
    DATA_SPLIT_FRACTIONS = (1.0, 0.0, 0.0)

    test_data, dummy1, dummy2 = import_and_partition(KNOWN_CLASSES, DATA_SPLIT_FRACTIONS,
                                                     SAMPLE_LENGTH, OVERLAP_AMOUNT, BATCH_SIZE)

    timbre_1 = TimbreNN.TimbreNN(test_data[1].shape[1], test_data[0].shape[1],
                                 SAMPLE_LENGTH, batch_size=BATCH_SIZE)

    # Log structure for TensorBoard after build()
    with tf.Session(graph=timbre_1.g) as sess:
        sess.run(timbre_1.init_op)
        file_writer = tf.summary.FileWriter(logdir='logs/', graph=timbre_1.g)

    timbre_1_model_weights = './model/timbre1_testbatch8_weights.hdf5'

    # TODO: change train/test/validation data into generators, allow for mutliprocessing in
    # evaluate()
    # (pc_scores, hamming, cm_data) = timbre_1.evaluate_simple(test_data[0], test_data[1],
    #                                     timbre_1_model_weights)
    (pc_scores, hamming, cm_data) = timbre_1.evaluate_custom(test_data[0], test_data[1],
                                        timbre_1_model_weights, show_plot=False,
                                        class_names=KNOWN_CLASSES)

if __name__ == '__main__':
    # TimbreTrain()
    TimbreHyperPScan()
