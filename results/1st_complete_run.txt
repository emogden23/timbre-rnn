Precision (manual): 0.8998427
Recall (manual): 0.89610296

Confusion matrix for class 1.0:
[[240470.  15171.]
 [ 35059. 619300.]]

Confusion matrix for class 2.0:
[[305397.  14408.]
 [ 27734. 562461.]]

Confusion matrix for class 3.0:
[[ 59369.  40594.]
 [  4573. 805464.]]
F1 score (test): 0.8979689285677447
Hamming loss (test): 0.05038058608058608
