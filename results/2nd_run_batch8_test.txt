Confusion matrix for class noise:
[[213588  15840]
 [ 17363 633209]]

Confusion matrix for class violin:
[[302722  14160]
 [ 43311 519807]]

Confusion matrix for class trumpet:
[[ 42342  41954]
 [ 13483 782221]]
Metrics for test data:
Per-class precision:  [0.9248 0.8748 0.7585]
Per-class recall:  [0.931  0.9553 0.5023]
Per-class F1 score:  [0.9279 0.9133 0.6044]
Hamming loss:  [0.0553]
