import numpy as np
# import vispy as vp
from vispy.plot import Fig

fig = Fig()
ax_left = fig[0, 0]
ax_right = fig[0, 1]
data = np.random.randn(2, 10)
ax_left.plot(data)
ax_right.histogram(data[1])
