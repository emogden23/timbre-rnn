
import glob
import os
import pdb
import pickle
import random
import sys
import tkinter as tk
from tkinter import filedialog
import zlib

import line_profiler
import pandas as pd
import numpy as np
import pywt

from TextTimesToTable import TextTimesToTable

# pywt.scale2frequency appears to be monotonic, so use bisection to find scales
def wavelet_s2f_bisection(wavelet, goal_freq, sample_prd, min_guess=1e-2, max_guess=1e6):
    """For evaluating how different wavelets affect scale-to-frequency conversion."""
    err_tol = 1e-6
    mid_freq = 1
    while abs(mid_freq) > err_tol:
        min_freq = pywt.scale2frequency(wavelet, min_guess)/sample_prd - goal_freq
        max_freq = pywt.scale2frequency(wavelet, max_guess)/sample_prd - goal_freq
        if not min_freq*max_freq < 0:
            sys.exit('Wavelet scale bisection failed: bounds have same sign')
        mid_guess = (min_guess + max_guess)/2
        mid_freq = pywt.scale2frequency(wavelet, mid_guess)/sample_prd - goal_freq
        if mid_freq*min_freq < 0:
            max_guess = mid_guess
        else:
            min_guess = mid_guess
    return mid_guess

def audio_extension_parse_files(file_list):
    # Since aubio is meant to be generic across extensions, can train from multiple file types
    # simultaneously. Need to make sure these multiple types are valid
    # TODO: not clear if python version has ffmpeg support, or if it requires rebuild; just assume
    # it does and wait for errors?
    audio_extensions = ['flac', 'm4a', 'mp3', 'ogg', 'wav']
    trimmed_file_list = []
    for filename in file_list:
        curr_extension = filename.split('.')[-1]
        if curr_extension in audio_extensions:
            trimmed_file_list.append(filename)

    return trimmed_file_list

# Handmade/unoptimized pipeline for initial testing, small datasets
def traditional_import(known_classes):
    # Import data, fail early if unmatched data
    tk_root = tk.Tk()
    tk_root.withdraw()
    # Since the input data is likely too big to use current drive, user defines it
    SGM_DIRNAME = tk.filedialog.askdirectory(title='Select the directory containing '
                                             'scaleogram *.npy files.')
    if not SGM_DIRNAME:
        sys.exit('No directory chosen - stopping...')

    SCGM_MAG_FILES = sorted(glob.glob(''.join((SGM_DIRNAME, '/mag_*.npy'))))

    # DEBUG; need to include brackets to keep it a list if only single file selected
    # SCGM_MAG_FILES = [SCGM_MAG_FILES[8]]

    mag_audio_filenames = []
    for mag_file in SCGM_MAG_FILES:
        mag_audio_filenames.append(extract_nested_audio_name(mag_file))

    # SCGM_PHASE_FILES = sorted(glob.glob(''.join((SGM_DIRNAME, '/phase_*.npy')))

    EVENT_FILES = sorted(glob.glob('audio_samples/parsed_onsets/cleaned/*.txt'))

    # DEBUG
    # EVENT_FILES = [EVENT_FILES[8]]

    class_audio_filenames = []
    for curr_file in EVENT_FILES:
        class_filename_parts_1 = curr_file.split('/')
        class_filename_parts_2 = class_filename_parts_1[-1].split('.')
        class_audio_filenames.append(class_filename_parts_2[0])

    if mag_audio_filenames != class_audio_filenames:
        sys.exit('Unmatched files in input ({0}) and/or output data ({1}) - stopping...'.format(
            np.setdiff1d(mag_audio_filenames, class_audio_filenames),
            np.setdiff1d(class_audio_filenames, mag_audio_filenames)))

    # Could lump all hstack's into single loop, but concerned about memory
    # allocation/fragmentation for large files - grow large, variable size arrays individually

    # HARDCODED: number of magnitude frequency bins (need to define non-concatenated dimensions)
    NUM_MAG_FEATURES = 120

    # Scaleogram cast up to 32-bit floats - lost information from calculations when storing,
    # but intermediate half as large to store; memory cost worth casting back, since
    # single precision FP much faster than half-precision FP on 1080 Ti
    mag_data_amalg = np.empty((NUM_MAG_FEATURES, 0), dtype=np.float32)    # input/'x'
    for mag_file in SCGM_MAG_FILES:
        mag_data_amalg = np.hstack((mag_data_amalg, np.load(mag_file)))

    # Normalize magnitude data separately from phase (different units, combined norm can cause problems)
    # Magnitude has hard minimum of zero, so only need to scale down from max
    mag_data_amalg /= np.max(mag_data_amalg)

    print('Magnitude data imported')

    # TODO: repeat logic (make function) for phase if valuable
    # Phase has hard mins/maxs, so just use them directly

    input_data = mag_data_amalg
    # input_data = np.vstack((mag_data_amalg, phase_data_amalg))

    # according to memory debugger, not needed - no new allocations, looks like a rename; may not
    # be true for mag+phase data
    # del mag_data_amalg

    class_data_amalg = np.empty((len(known_classes), 0), dtype=np.uint8)    # output/'y'
    text_expand_success = True
    for curr_file in EVENT_FILES:
        expanded_data, curr_success = TextTimesToTable(curr_file, known_classes, False)
        class_data_amalg = np.hstack((class_data_amalg, expanded_data.T))
        text_expand_success = text_expand_success and curr_success

    if not text_expand_success:
        sys.exit('Imported class label data contains unknown instruments, see console')

    if not np.all(np.logical_or(class_data_amalg == 0, class_data_amalg == 1)):
        sys.exit('Imported class labels have values besides 0 or 1 - stopping...')

    print('Class labels imported')

    output_data = class_data_amalg

    # del class_data_amalg

    # TODO: evaluate data to see if class imbalance is present; if so, then what?

    # Even if all files included, make sure number of samples are matched
    if input_data.shape[1] != output_data.shape[1]:
        sys.exit('Mismatch between number of input ({0}) and output ({1}) samples - stopping...'.format(
            input_data.shape[1], output_data.shape[1]))

    tk_root.destroy()

    return input_data, output_data


def pipeline_import(known_classes):
    # TODO: should this just be a different way of storing/importing files, or should this
    # be a 'more complete' data processing pipeline (i.e., generate wavelet data from input
    # audio file also)?

    # Could import audio file, use OfflineAudioToSpectWavelet to generate input data (but not
    # Offline...OnsetDetect - not good enough to run independently, needs manual cleaning), 2D
    # np.array added to dataset using from_tensor_slices
    sys.exit('Incomplete, do not use yet')

    import tkinter as tk
    from tkinter import filedialog

    # TODO: get directories for input, output at start (if this is long, don't want another
    # prompt buried in the middle)
    # TODO: similar enough to traditional_import, make this a subfunction?

    tk_root = tk.Tk()
    tk_root.withdraw()
    # Since the input data is likely too big to use current drive, user defines it
    SGM_DIRNAME = tk.filedialog.askdirectory(title='Select the directory containing '
                                             'scaleogram *.npy files.')
    if not SGM_DIRNAME:
        sys.exit('No directory chosen - stopping...')

    FRAMES_DIRNAME = tk.filedialog.askdirectory(title='Select the directory containing '
                                             'output ''text-times'' files.')
    if not FRAMES_DIRNAME:
        sys.exit('No directory chosen - stopping...')

    # Get filenames, same as traditional_import
    SCGM_MAG_FILES = sorted(glob.glob(''.join((SGM_DIRNAME, '/mag_*.npy'))))

    # DEBUG; need to include brackets to keep it a list if only single file selected
    # SCGM_MAG_FILES = [SCGM_MAG_FILES[8]]

    mag_audio_filenames = []
    for mag_file in SCGM_MAG_FILES:
        mag_audio_filenames.append(extract_nested_audio_name(mag_file))

    # SCGM_PHASE_FILES = sorted(glob.glob(''.join((SGM_DIRNAME, '/phase_*.npy')))

    EVENT_FILES = sorted(glob.glob('audio_samples/parsed_onsets/cleaned/*.txt'))

    # DEBUG
    # EVENT_FILES = [EVENT_FILES[8]]

    class_audio_filenames = []
    for curr_file in EVENT_FILES:
        class_filename_parts_1 = curr_file.split('/')
        class_filename_parts_2 = class_filename_parts_1[-1].split('.')
        class_audio_filenames.append(class_filename_parts_2[0])

    if mag_audio_filenames != class_audio_filenames:
        sys.exit('Unmatched files in input ({0}) and/or output data ({1}) - stopping...'.format(
            np.setdiff1d(mag_audio_filenames, class_audio_filenames),
            np.setdiff1d(class_audio_filenames, mag_audio_filenames)))

    # TODO

# Wrapper function for import-and-split reduces memory allocated before training
def import_and_partition(known_classes, data_split_fractions, sample_length, overlap_amount,
                         batch_size=1):
    input_data, output_data = traditional_import(known_classes)

    # TODO: redo this function to reduce memory use?
    train_data, test_data, validation_data = partition_data(input_data, output_data,
                                                            data_split_fractions, sample_length,
                                                            overlap_amount, batch_size)
    return train_data, test_data, validation_data

# Mostly from CharRNN code 'ch16/create_batch_generator' in
# "Python Machine Learning, 2nd ed" by Raschka and Mirjalili
def yield_batches(data_x, data_y, sample_length, overlap_amount, num_batches=0, randomize=False):
    # TODO: does this have persistent state per generator object? If so, can use one generator
    # to get train, test, and validation data more easily?
    batch_width, total_length = data_x.shape
    sample_start_diff = sample_length - overlap_amount

    # Provide num_batches if partial set desired (like matching batch_size for training)
    if not num_batches:
        num_batches = (total_length - sample_length)//(sample_start_diff) + 1
    batch_order = [*range(num_batches)]  # can't shuffle lazy-initialized range

    if randomize:
        random.shuffle(batch_order)
    for b in batch_order:
        min_index = b*sample_start_diff
        yield (data_x[:, min_index: min_index + sample_length],
               data_y[:, min_index: min_index + sample_length])

# simple implementation: https://stackoverflow.com/a/13082705
def round_down(val, divisor):
    return val - (val%divisor)

# @profile
def partition_data(input_data, output_data, data_split_fractions, sample_len, overlap_amt,
                   training_batch_size=1, predict_only=False):
    # TODO: seems like I'm undoing the benefits of the generator expression and
    # duplicating the memory use
    train_fraction, test_fraction, validation_fraction = data_split_fractions

    # Adjust indices to ensure all data sets (train, test, validation) have batch sizes
    # that are multiples of training/design value
    num_batches = round_down((input_data.shape[1] - sample_len)//(sample_len - overlap_amt) + 1,
                             training_batch_size)
    test_index = round_down(int(num_batches*(1 - test_fraction)), training_batch_size)
    validation_index = round_down(int(num_batches*(1 - test_fraction - validation_fraction)),
                                  training_batch_size)

    if test_index == validation_index or validation_index == 0:
        print('Test fraction, validation fraction, and/or batch size resulted in '
                 'zero-length data partition. Training may behave oddly.')

    # Not generic to 4+-D data...
    batched_input = np.zeros((num_batches, input_data.shape[0], sample_len),
                                    dtype=np.float32)
    batched_output = np.zeros((num_batches, output_data.shape[0], sample_len),
                                    dtype=np.uint8)

    # Data order should be kept for predictions
    batch_gen = yield_batches(input_data, output_data, sample_len, overlap_amt, num_batches,
                              randomize=not predict_only)
    i = 0
    for batch in batch_gen:
        batched_input[i] = batch[0]
        batched_output[i] = batch[1]
        i += 1

    return ((batched_input[:validation_index, :, :],
                batched_output[:validation_index, :, :]),
            (batched_input[validation_index:test_index, :, :],
                batched_output[validation_index:test_index, :, :]),
            (batched_input[test_index:, :, :], batched_output[test_index:, :, :]))

def extract_nested_audio_name(fullpath):
    filename_parts_1 = fullpath.split('/')
    filename_parts_2 = filename_parts_1[-1].split('_')
    filename_parts_3 = '_'.join((filename_parts_2[1:]))
    base_filename = filename_parts_3.split('.')[0]
    # print(filename_parts_1)
    # print(filename_parts_2)
    # print(base_filename)
    return base_filename

def flatten_batched_tensor_to_2D(input):
    # TODO: not generic/tested for N-D tensors, designed for 3D "batches x classes x timesteps"
    output = np.zeros((input.shape[1], int(input.size/input.shape[1])))
    for i in range(input.shape[0]):
        output[:, i*input.shape[2]:(i+1)*input.shape[2]] = input[i, :, :]

    return output

def binary_round_by_threshold(data, threshold):
    rounded_data = np.zeros(data.shape, dtype=data.dtype)
    high_inds = data > threshold
    rounded_data[high_inds] = 1
    # since initialized to 0, assignment as 0 not needed
    # rounded_data[np.logical_not(high_inds)] = 0

    return rounded_data

def calc_confusion_mats(y_true, y_pred, rounding_threshold=0.5, class_names='',
                        print_results=False):
    if y_true.shape != y_pred.shape:
        sys.exit('True and predicted output data not the same size - stopping...')

    # Since multilabel, each class is like an output - flatten to 1D? Doesn't help identify
    # which classes may be performing poorly
    # y_true_flat = y_true.flatten()
    # y_pred_flat = y_pred.flatten()

    # Reduce batched y data to 2D - allows for calculating performance of each class/row
    num_classes = y_true.shape[1]

    y_true_2D = flatten_batched_tensor_to_2D(y_true)
    y_pred_2D = flatten_batched_tensor_to_2D(y_pred)

    y_pred_2D = binary_round_by_threshold(y_pred_2D, rounding_threshold)

    # TODO: don't have anything for null/silence class
    true_1 = y_true_2D == 1
    true_0 = np.logical_not(true_1)
    pred_1 = y_pred_2D == 1
    pred_0 = np.logical_not(pred_1)
    true_pos = np.sum(np.logical_and(pred_1, true_1), axis=1)
    true_neg = np.sum(np.logical_and(pred_0, true_0), axis=1)
    false_pos = np.sum(np.logical_and(pred_1, true_0), axis=1)
    false_neg = np.sum(np.logical_and(pred_0, true_1), axis=1)

    # DEBUG: Scores/values: from Ch 6 of Python Machine Learning, 2nd ed
    # Hamming loss: based on Wikipedia and https://stats.stackexchange.com/a/168952 ,
    # sum up all false positives and negatives (give true output of XOR), divive by total
    # number of labels
    # pred_error = (false_pos + false_neg)/(false_pos + false_neg + true_pos + true_neg)
    # pred_acc = 1 - pred_error
    precision = true_pos/(true_pos + false_pos)
    recall = true_pos/(false_neg + true_pos)
    F1_score = 2*(precision*recall)/(precision + recall)
    # hamming_loss = (sum(false_neg) + sum(false_pos))/y_true_2D.size

    # TODO: micro/macro precision?

    if print_results:
        if not class_names:
            class_names = np.zeros(num_classes)
            for i in range(num_classes):
                class_names[i] = int(i) + 1
        for i in range(num_classes):
            print('\nConfusion matrix for class {0}:'.format(class_names[i]))
            print(np.array([[true_pos[i], false_neg[i]], [false_pos[i], true_neg[i]]]))

    return ((true_pos, true_neg, false_pos, false_neg), (precision, recall, F1_score))


def calc_conf_mat_metrics(test_y, pred_y, rounding_threshold, class_names='', print_results=False):
    (conf_mat_data, per_class_scores) = calc_confusion_mats(test_y, pred_y,
                                                            rounding_threshold,
                                                            class_names, print_results)
    (tp, tn, fp, fn) = conf_mat_data
    (pc_precision, pc_recall, pc_F1) = per_class_scores

    hamming_loss = (sum(fn) + sum(fp))/test_y.size

    if print_results:
        with np.printoptions(precision=4):
            print('Metrics for test data:')
            print('Per-class precision: ', pc_precision)
            print('Per-class recall: ', pc_recall)
            print('Per-class F1 score: ', pc_F1)
            print('Hamming loss: ', np.array([hamming_loss]))

    return (per_class_scores, hamming_loss, conf_mat_data)

def serialized_params_to_index(params, filename):
    # Trim parameters not tied to model weights/architecture (optimizer, batch size);
    # TODO: still feel uneasy about all of this, just remove num_epochs for now
    trimmed_params = params.copy()
    params_to_trim = [
                      # 'learning_rate',
                      # 'grad_clip',
                      # 'rounding_threshold',
                      # 'optimizer',
                      'epochs',
                      # 'batch_size'
                     ]
    for key in params_to_trim:
        trimmed_params.pop(key, None)

    param_string = zlib.compress(pickle.dumps(trimmed_params, 4))

    # TODO: list the params dictionary to clarify which configuration is active?
    if os.path.isfile(filename):
        with open(filename, 'rb') as param_file:
            param_list = pickle.load(param_file)
            # TODO: this can probably be more efficient - if param_list.index() throws a ValueError,
            # it's not in the list - only have to do one search
            if param_string not in param_list:
                print('New parameter configuration, adding to list')
                index = len(param_list)
                param_list.append(param_string)
            else:
                print('Existing parameter configuration, retrieving from list')
                index = param_list.index(param_string)
    else:
        print('Creating list of parameter configurations')
        param_list = [param_string]
        index = 0

    with open(filename, 'wb') as param_file:
        pickle.dump(param_list, param_file, 4)

    return index
